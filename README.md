## Editor WYSIWYG ##

Éditeur WYSIWYG (What You See Is What You Get : littéralement en français "ce que vous voyez est ce que vous obtenez") basé sur CKEditor et compatible avec la majorité des navigateurs :
* Internet Explorer à partir de la version 8
* Firefox à partir de la version 3
* Chrome, la dernière version stable
* Opera, la dernière version stable

Cette interface intuitive permet donc la saisie de texte enrichi avec les fonctions classiques de traitement de texte tout en proposant un rendu visuel instantané. Il intègre également un correcteur orthographique ainsi que les fonctionnalités spécifiques à Ovidentia.
[En savoir plus sur CKEditor](http://ckeditor.com/)