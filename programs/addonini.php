;<?php/*

[general]
name                        ="LibCkEditor"
version                     ="2.9.0"
addon_type                  ="LIBRARY"
encoding                    ="UTF-8"
mysql_character_set_database="latin1,utf8"
description                 ="CkEditor, an open source WYSIWYG text editor"
description.fr              ="Librairie partagée fournissant un éditeur WYSIWYG pour les champs de saisie de texte"
long_description.fr         ="README.md"
delete                      =1
ov_version                  ="8.1.100"
php_version                 ="5.1.0"
author                      ="Antoine Gallet ( antoine.gallet@cantico.fr )"
icon                        ="icon.png"
addon_access_control        ="0"
configuration_page          ="configuration"
db_prefix                   ="lcke_"
tags                        ="library,editor,default"

[addons]
widgets="0.2.12"

[functionalities]
jquery="Available"
;*/?>