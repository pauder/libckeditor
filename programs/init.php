<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once 'base.php';

/**
 * Fonction appellée au moment de la mise à jour et de l'installation de l'addon
 */
function LibCkEditor_upgrade($version_base,$version_ini)
{
	$addonInfo = bab_getAddonInfosInstance('LibCkEditor');
	
	$uPath = new bab_Path($addonInfo->getUploadPath());
	if(!$uPath->isDir()){
		$uPath->createDir();
		$uPath->push('styles.js');
		if(!is_file($uPath->tostring())){
			$ourFileHandle = fopen($uPath->tostring(), 'w');
			if($ourFileHandle){
				fclose($ourFileHandle);
			}
		}
	}
	
	if (!bab_isTable("lcke_styles")) {
		$sql = "CREATE TABLE `lcke_styles` (
				  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `name` VARCHAR( 255 ) NOT NULL,
				  `class` VARCHAR( 255 ) NOT NULL);";
		$GLOBALS['babDB']->db_query($sql);
	}
	
	

    $addon = bab_getAddonInfosInstance('LibCkEditor');
    $addon->removeAllEventListeners();
    $addon->addEventListener('bab_eventEditorContentToEditor',	'ckeditor_onContentToEditor',	'events.php', 350);
	$addon->addEventListener('bab_eventEditorRequestToContent',	'ckeditor_onRequestToContent',	'events.php', 350);
	$addon->addEventListener('bab_eventEditorContentToHtml',	'ckeditor_onContentToHtml',		'events.php', 350);


	return true;
}

/**
 * Fonction appellée au moment de la suppression de l'addon
 */
function LibCkEditor_onDeleteAddon()
{	
	// detach events
	if(function_exists('bab_removeAddonEventListeners')){
	    bab_removeAddonEventListeners('LibCkEditor');
	} else {
		include_once $GLOBALS['babInstallPath']."utilit/eventincl.php";
		bab_removeEventListener('bab_eventEditorContentToEditor'	,'ckeditor_onContentToEditor'	,'addons/LibCkEditor/events.php');
		bab_removeEventListener('bab_eventEditorRequestToContent'	,'ckeditor_onRequestToContent'	,'addons/LibCkEditor/events.php');
		bab_removeEventListener('bab_eventEditorContentToHtml'		,'ckeditor_onContentToHtml'		,'addons/LibCkEditor/events.php');
	}
	
	return true;
}


?>
